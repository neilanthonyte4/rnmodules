//
//  WifiModule.swift
//  rnmodules
//
//  Created by Neil Anthony Te on 3/29/22.
//

import Foundation

@objc(WifiModule)

class WifiModule: NSObject, RCTBridgeModule{
  
  static func moduleName() -> String! {
    return "WifiModule";
  }
  
  static func requiresMainQueueSetup() -> Bool {
    return true;
  }
  
  @objc
  func scanWifi(_ callback: RCTResponseSenderBlock) -> Void {
    let resultsDict = [
      "success" : true
    ];
         
    callback([resultsDict, NSNull()])
  }
  
  @objc
  func connect(_ name: String) -> Void {
    NSLog("%@", name);
  }
}
