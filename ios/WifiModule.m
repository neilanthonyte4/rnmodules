//
//  WifiModule.m
//  rnmodules
//
//  Created by Neil Anthony Te on 3/29/22.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(WifiModule, NSObject)

RCT_EXTERN_METHOD(scanWifi: (RCTResponseSenderBlock)callback)

@end
