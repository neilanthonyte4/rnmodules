package com.rnmodules;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.uimanager.IllegalViewOperationException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

import java.util.List;
import android.util.Log;

public class WifiModule extends ReactContextBaseJavaModule {
    WifiModule(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "WifiModule";
    }

    WifiManager wifiManager = (WifiManager) getReactApplicationContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

    BroadcastReceiver wifiScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean success = intent.getBooleanExtra(
                    WifiManager.EXTRA_RESULTS_UPDATED, true);

            if (success) {
                scanSuccess();
            } else {
                scanFailure();
            }
        }
    };

    private WritableArray scanSuccess() {
        List<ScanResult> results = wifiManager.getScanResults();
        WritableArray wifiArray =  Arguments.createArray();
        for (ScanResult result: results) {
            if(!result.SSID.equals("")){
                wifiArray.pushString(result.SSID);
            }
        }

        return wifiArray;
    }

    private WritableArray scanFailure() {
        List<ScanResult> results = wifiManager.getScanResults();
        WritableArray wifiArray =  Arguments.createArray();
        for (ScanResult result: results) {
            if(!result.SSID.equals("")){
                wifiArray.pushString(result.SSID);
            }
        }

        return wifiArray;
    };

    @ReactMethod
    public void scanWifi(Callback successCallback, Callback errorCallback) {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
            getReactApplicationContext().getApplicationContext().registerReceiver(wifiScanReceiver, intentFilter);

            boolean success = wifiManager.startScan();

            if (!success) {
                WritableArray scanRes = scanFailure();
                successCallback.invoke(scanRes);
            } else {
                WritableArray scanRes = scanSuccess();
                successCallback.invoke(scanRes);
            }

        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }



    @ReactMethod
    public void connect() {

    }
}
