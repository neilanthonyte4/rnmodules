package com.rnmodules;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.util.Map;
import java.util.HashMap;
import android.util.Log;

public class RNRadialGradientModule extends ReactContextBaseJavaModule {
  
    private final ReactApplicationContext reactContext;

    RNRadialGradientModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @ReactMethod
    public void myNativeFn() {
        Log.d("RNRadialGradientModule", "Hi I am a native function");
    }

    @Override
    public String getName() {
        return "RNRadialGradientModule";
    }
}