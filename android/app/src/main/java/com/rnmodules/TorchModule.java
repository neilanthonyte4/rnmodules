package com.rnmodules;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.util.Map;
import java.util.HashMap;
import android.util.Log;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.ReactApplication;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import com.facebook.react.bridge.Promise;

public class TorchModule extends ReactContextBaseJavaModule {
   private CameraManager mCameraManager;
   private String mCameraId;
   private ToggleButton toggleButton;

    TorchModule(ReactApplicationContext context) {
        super(context);
    }

    @ReactMethod
    public void toggleTorch(Promise promise) {
        WritableMap resultData = Arguments.createMap();
        // boolean isFlashAvailable = rnApp.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);

        boolean isFlashAvailable = false;

        try {
            if (!isFlashAvailable) {
                resultData.putString("error", "no torch available");
                promise.resolve(resultData);
            } else {
                resultData.putString("success", "torch available");
                promise.resolve(resultData);
            }
        } catch (Exception e) {
            promise.resolve(e);
        }
    }

    @Override
    public String getName() {
        return "TorchModule";
    }
}
