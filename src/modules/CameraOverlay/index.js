import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image,
  NativeModules,
} from 'react-native';
import FlashEnabled from './assets/flash_enabled.png';
import FlashDisabled from './assets/flash_disabled.png';
import Torch from 'react-native-torch';

// const {toggleTorch} = NativeModules.TorchModule;

const {width, height} = Dimensions.get('window');
const VERTICAL_LAYER_HEIGHT = height * 0.2;
const HORIZONTAL_LAYER_HEIGHT = width * 0.075;

const CameraOverlay = () => {
  const [torchEnabled, setTorchStatus] = useState(false);

  const _onToggleTorch = async () => {
    try {
      await Torch.switchState(!torchEnabled);
      setTorchStatus(!torchEnabled);
    } catch (error) {
      console.log('error', error);
    }
  };

  return (
    <ImageBackground
      source={{
        uri: 'https://img.freepik.com/free-photo/beautiful-view-greenery-bridge-forest-perfect-background_181624-17827.jpg',
      }}
      style={Styles.container}
      resizeMode="cover">
      <TouchableOpacity style={Styles.torchToggle} onPress={_onToggleTorch}>
        <Image
          source={torchEnabled ? FlashEnabled : FlashDisabled}
          resizeMode="contain"
          style={Styles.torchIcon}
        />
      </TouchableOpacity>
      <View style={Styles.topLayer}></View>
      <View style={Styles.bottomLayer}></View>
      <View style={Styles.leftLayer}></View>
      <View style={Styles.rightLayer}></View>
      <View style={Styles.lensBorderTopLeft}></View>
      <View style={Styles.lensBorderTopRight}></View>
      <View style={Styles.lensBorderBottomRight}></View>
      <View style={Styles.lensBorderBottomLeft}></View>
    </ImageBackground>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    overflow: 'hidden',
  },
  topLayer: {
    width: width - HORIZONTAL_LAYER_HEIGHT * 2,
    height: VERTICAL_LAYER_HEIGHT,
    backgroundColor: 'rgba(0,0,0,0.5)',
    position: 'absolute',
    top: 0,
    left: HORIZONTAL_LAYER_HEIGHT,
    zIndex: 9,
  },
  bottomLayer: {
    width: width - HORIZONTAL_LAYER_HEIGHT * 2,
    height: VERTICAL_LAYER_HEIGHT,
    backgroundColor: 'rgba(0,0,0,0.5)',
    position: 'absolute',
    bottom: 0,
    left: HORIZONTAL_LAYER_HEIGHT,
    zIndex: 9,
  },
  leftLayer: {
    width: HORIZONTAL_LAYER_HEIGHT,
    height: height,
    backgroundColor: 'rgba(0,0,0,0.5)',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 9,
  },
  rightLayer: {
    width: HORIZONTAL_LAYER_HEIGHT,
    height: height,
    backgroundColor: 'rgba(0,0,0,0.5)',
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 9,
  },
  lensBorderTopLeft: {
    position: 'absolute',
    width: 60,
    height: 60,
    backgroundColor: 'rgba(0,0,0,0)',
    zIndex: 9,
    top: VERTICAL_LAYER_HEIGHT,
    left: HORIZONTAL_LAYER_HEIGHT,
    borderTopWidth: 5,
    borderLeftWidth: 5,
    borderColor: '#fff',
  },
  lensBorderTopRight: {
    position: 'absolute',
    width: 60,
    height: 60,
    backgroundColor: 'rgba(0,0,0,0)',
    zIndex: 9,
    top: VERTICAL_LAYER_HEIGHT,
    right: HORIZONTAL_LAYER_HEIGHT,
    borderTopWidth: 5,
    borderRightWidth: 5,
    borderColor: '#fff',
  },
  lensBorderBottomRight: {
    position: 'absolute',
    width: 60,
    height: 60,
    backgroundColor: 'rgba(0,0,0,0)',
    zIndex: 9,
    bottom: VERTICAL_LAYER_HEIGHT,
    left: HORIZONTAL_LAYER_HEIGHT,
    borderBottomWidth: 5,
    borderLeftWidth: 5,
    borderColor: '#fff',
  },
  lensBorderBottomLeft: {
    position: 'absolute',
    width: 60,
    height: 60,
    backgroundColor: 'rgba(0,0,0,0)',
    zIndex: 9,
    bottom: VERTICAL_LAYER_HEIGHT,
    right: HORIZONTAL_LAYER_HEIGHT,
    borderBottomWidth: 5,
    borderRightWidth: 5,
    borderColor: '#fff',
  },
  torchToggle: {
    width: 40,
    height: 40,
    position: 'absolute',
    zIndex: 10,
    top: 20,
    right: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  torchIcon: {
    width: 40,
    height: 40,
  },
});

export default CameraOverlay;
