import React from 'react';
import {View, Modal, Text, TouchableOpacity} from 'react-native';
import useStyle from './index.styles';
import DateRangePicker from "react-native-daterange-picker";
import moment from "moment";

const RickDateRangePickerUI = ({show = false, _onApplyDate, _onSelectDate}) => {
    const Styles = useStyle();
    const [selectedDate, setSelectedDate] = React.useState(null);

    const _onChangeDate = (date) => { 
        _onSelectDate(date);
        setSelectedDate(date);
    }

    const _onPressRightHeader = () => {
        alert('ekker dako ug notch');
    }

    const _onPressLeftHeader = () => {
        alert('ekker ibog nessa');
    }

    return (
        <Modal
            animationType='slide'
            transparent={true}
            visible={show}
            onRequestClose={() => {}}
        >
            <View style={Styles.container}>
                <View style={Styles.picker_wrapper}>
                    <View style={Styles.picker_header}>
                        <View style={Styles.header_section_left}>
                            <Text style={Styles.left_header_label} onPress={_onPressRightHeader}>Select date range</Text>
                        </View>
                        <View style={Styles.header_section_right}>
                            <Text style={Styles.right_header_label} onPress={_onPressLeftHeader}>This week</Text>
                        </View>
                    </View>

                    <View style={{flex: 1}}>
                        <DateRangePicker
                            onChange={_onChangeDate}
                            endDate={null}
                            startDate={null}
                            displayedDate={moment()}
                            open={true}
                            backdropStyle={Styles.picker_backdrop}
                            containerStyle={Styles.picker_container}
                        ><Text></Text></DateRangePicker>
                    </View>

                    <TouchableOpacity style={Styles.apply_btn} onPress={() => _onApplyDate(selectedDate)}>
                        <Text style={Styles.btn_label}>Apply</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

export default RickDateRangePickerUI;