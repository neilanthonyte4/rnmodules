import {StyleSheet, Dimensions} from 'react-native';

const useStyle = () => {
    const {width, height} = Dimensions.get('window');
    const PICKER_WIDTH = width * 0.9;
    const PICKER_HEIGHT = height * 0.7;
    const PICKER_HEADER_HEIGHT = PICKER_HEIGHT * 0.1;
    const PICKER_CALENDAR_CONTAINER = PICKER_HEIGHT - (PICKER_HEIGHT * 0.25);

    return StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(0,0,0,0.5)'
        },
        picker_wrapper: {
            width: PICKER_WIDTH,
            height: PICKER_HEIGHT,
            backgroundColor: '#fff',
            borderRadius: 10,
            overflow: 'hidden',
            justifyContent: 'center',
            alignItems: 'center',
        },
        picker_header: {
            width: PICKER_WIDTH,
            height: PICKER_HEADER_HEIGHT,
            borderBottomWidth: 1,
            borderColor: '#d1d1d1',
            flexDirection: 'row'
        },
        header_section_left: {
            justifyContent: 'center',
            alignItems: 'flex-start',
            flex: 1,
            paddingLeft: 10
        },
        header_section_right: {
            justifyContent: 'center',
            alignItems: 'flex-end',
            flex: 1,
            paddingRight: 10
        },
        left_header_label: {
            fontWeight: 'bold'
        },
        right_header_label: {
            fontWeight: 'bold',
            color: '#0099ff'
        },
        apply_btn: {
            backgroundColor: '#1670f7',
            width: PICKER_WIDTH * 0.9,
            height: 50,
            marginBottom: 15,
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
        },
        btn_label: {
            fontWeight: 'bold',
            color: '#fff'
        },
        picker_container: {},
        picker_backdrop: {
            backgroundColor: '#fff',
            position: 'relative',
            top: 0,
            left: 0,
            width: PICKER_WIDTH,
            height: PICKER_CALENDAR_CONTAINER

        }
    });
}

export default useStyle;