import {StyleSheet} from 'react-native';

const useStyle = () => {
    return StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },
        show_datepicker_btn: {
            padding: 10,
            backgroundColor: 'red',
            borderRadius: 10
        },
        show_datepicker_label: {
            color: '#fff'
        }
    })
}

export default useStyle;