import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import useStyle from './implementation.styles';
import RickDateRangePickerUI from './index';
import moment from 'moment';

const RickDatePickerImplementation = () => {
    const Styles = useStyle();
    const [show, setShow] = React.useState(false);
    const [selecteDate, setSelectedDate] = React.useState(null);

    const _onSelectDate = ({date}) => {
        setSelectedDate(date);
    }

    const _onApplyDate = ({date}) => {
        setSelectedDate(date);
        setShow(false)
    }

    return (
        <View style={Styles.container}>
            <Text>Selectedm Date: {!selecteDate ? null : moment(selecteDate).format('MM DD YYYY')}</Text>
            <TouchableOpacity style={Styles.show_datepicker_btn} onPress={() => setShow(!show)}>
                <Text style={Styles.show_datepicker_label}>Show Date Picker</Text>
            </TouchableOpacity>

            <RickDateRangePickerUI {...{show, _onApplyDate, _onSelectDate}} />
        </View>
    )
}

export default RickDatePickerImplementation;