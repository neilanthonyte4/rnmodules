## # RickDateRangePickerUI
RickDateRangePickerUI is a datepicker based on https://github.com/naxulanth/react-native-daterange-picker/ but wrapped in a custom UI that has Apply button and a header. 

See the [RickDatePickerImplementation](https://gitlab.com/neilanthonyte4/rnmodules/-/blob/main/src/modules/RickDatePicker/implementation.js "RickDatePickerImplementation") for reference.

### Screenshot Android
![](https://drive.google.com/uc?id=1hS9caTkLAJrzZOuY7LqPLuOYI-WV3WW8)

### Screenshot iOS
![](https://drive.google.com/uc?id=1P32eWNxhycftpycacPRRTpXKZm703qGY)