import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import useStyles from './index.styles';
import moment from 'moment';
import _ from 'lodash';

const UserTaskAssignment = () => {
    const Styles = useStyles();
    const [taskToBeMoved, setTaskToBeMoved] = React.useState(null);

    // THIS IS BASED ON THE SELECTED DATE RANGE FROM DATE PICKER
    const [tasks, setTask] = React.useState([
        {day: 'indicator'},
        {day: 'Mon', date: 18, pendingTask: null},
        {day: 'Tue', date: 19, pendingTask: null},
        {day: 'Wed', date: 20, pendingTask: null},
        {day: 'Thu', date: 21, pendingTask: null},
        {day: 'Fri', date: 22, pendingTask: null},
        {day: 'Sat', date: 23, pendingTask: null},
        {day: 'Sun', date: 24, pendingTask: null}
    ]);

    // THIS ARE THE USERS BASED FROM THE BACKEND
    // IT IS NEEDED THAT WE HAVE A FIELD CALLED AVAILABILITY WHERE WE CAN SEE
    // WHAT DATE THE USER IS AVAILABLE AND WHAT TASK THAT DAY HE/SHE IS ASSIGNED
    const [users, setUser] = React.useState([
        {name: 'user1', available: [{date: 'Mon-18', assignedTask: null}, {date: 'Wed-20', assignedTask: null}]},
        {name: 'user2', available: [{date: 'Thu-21', assignedTask: null}, {date: 'Sun-24', assignedTask: null}]},
        {name: 'user3', available: [{date: 'Fri-22', assignedTask: null}, {date: 'Tue-19', assignedTask: null}]},
        {name: 'user4', available: [{date: 'Sat-23', assignedTask: null}, {date: 'Sun-24', assignedTask: null}]},
        {name: 'user5', available: [{date: 'Mon-18', assignedTask: null}, {date: 'Sat-23', assignedTask: null}, {date: 'Sun-24', assignedTask: null}]}
    ]);

    // ADDING OF TASK
    const _onAddTask = (taskInfo) => {
        return () => {
            let newTasks = [...tasks];
            const getIndex = _.findIndex(newTasks, taskInfo);
            newTasks[getIndex].pendingTask = 'some task added';
            setTask(newTasks);
        }
    }

    // SET THE TASK TO BE MOVED
    // THIS WILL BE DELETED ONCE THE TASK HAS BEEN MOVED SUCCESSFULLY
    // TEMP DATA HOLDER
    // THIS WILL ALSO TRIGGER TO SHOW ALL THE AVAILABLE SLOTS WHERE THE
    // TASK CAN BE MOVED
    const _onMoveTask = (task) => {
        return () => {
            setTaskToBeMoved(task);
        }
    }

    // THE SETTING OF TASK FROM ONE SLOT TO ANOTHER
    const _onSetTask = (isAvailable, user, currentPendingTask, taskToMove) => {
        return () => {
            if(isAvailable) {
                let newUsers = [...users];
                let newTasks = [...tasks];

                const getIndex = _.findIndex(newUsers, user);
                const getTaskIndex = _.findIndex(newTasks, taskToMove);

                let {available} = newUsers[getIndex];

                for(let a = 0; a < available.length; a+=1) {
                    let {date} = available[a];

                    const getAveInfo = date.split('-');
                    const aveDay = getAveInfo[0];
                    const aveDate = Number(getAveInfo[1]);

                    if(currentPendingTask.day === aveDay && currentPendingTask.date === aveDate) {
                        newUsers[getIndex].available[a].assignedTask = taskToMove.pendingTask;
                        newTasks[getTaskIndex].pendingTask = null;
                
                        setTaskToBeMoved(null);
                        setTask(newTasks);
                        setUser(newUsers);
                    }
                }
            }
        }
    }

    return (
        <View style={Styles.container}>
            <View style={Styles.days_container}>
            {
                tasks.map((d, i) => {

                    // ROW INDICATOR
                    if(d.day === 'indicator') {
                        return (
                            <View key={'day-indicator-'+i} style={Styles.day_container}>
                                <Text>Date</Text>
                            </View>
                        )
                    }

                    return (
                        <View key={'day-month-'+i} style={Styles.day_container}>
                            <Text>{`${d.day} \n ${d.date}`}</Text>
                        </View>
                    )
                })
            }
            </View>

            <View style={Styles.add_task_btn_container}>
            {
                tasks.map((d, i) => {

                    // ROW INDICATOR
                    if(d.day === 'indicator') {
                        return (
                            <View key={'add-indicator-'+i} style={Styles.day_container}>
                                
                            </View>
                        )
                    }

                    return (
                        <TouchableOpacity key={'add-task-'+i} onPress={_onAddTask(d)}>
                            <View style={Styles.day_container}>
                                <Text>+</Text>
                            </View>
                        </TouchableOpacity>
                    )
                })
            }
            </View>

            <View style={Styles.task_indicator_container}>
            {
                tasks.map((d, i) => {

                    // ROW INDICATOR
                    if(d.day === 'indicator') {
                        return (
                            <View key={'unassigned-indicator-'+i} style={Styles.outter_outter_continer}>
                                
                            </View>
                        )
                    }

                    return (
                        <View key={'unassigned-task-'+i} style={Styles.outter_outter_continer}>
                            <TouchableOpacity style={Styles.task_outter_container(d.pendingTask)} onPress={_onMoveTask(d)}>
                                <View style={Styles.task_container(d.pendingTask)}>
                                    {d.pendingTask ? <Text>{d.pendingTask}</Text> : null}
                                </View>
                            </TouchableOpacity>
                        </View>
                    )
                })
            }
            </View>

            {
                users.map((u, ui) => {
                    return (
                        <View key={'user-'+ui} style={Styles.user_list_item}>
                            <View style={Styles.task_indicator_container}>
                            {
                                tasks.map((d, i) => {
                                    const userAvailableDay = u.available;
                                    let isAvailable = false;
                                    let aTask = null
    
                                    if(userAvailableDay && userAvailableDay.length > 0) {
                                        for(let a = 0; a < userAvailableDay.length; a+=1) {
                                            const {date, assignedTask} = userAvailableDay[a];
    
                                            const getAveInfo = date.split('-');
                                            const aveDay = getAveInfo[0];
                                            const aveDate = Number(getAveInfo[1]);
    
                                            if(d.day === aveDay && d.date === aveDate) {
                                                aTask = assignedTask;
                                                isAvailable = true;
                                            }
                                        }
                                    }

                                    // ROW INDICATOR
                                    if(d.day === 'indicator') {
                                        return (
                                            <View key={'user_indicator-'+i} style={Styles.user_outter_container}>
                                                <Text>{u.name}</Text>
                                            </View>
                                        )
                                    }
                                    
                                    if(aTask && isAvailable) {
                                        return (
                                            <View key={'assigned-task-'+i} style={Styles.user_outter_container}>
                                                <View style={Styles.task_container(aTask, '#99ebff')}>
                                                    {aTask ? <Text>{aTask}</Text> : null}
                                                </View>
                                            </View>
                                        )
                                    }
    
                                    if(taskToBeMoved && isAvailable) {
                                        return (
                                            <TouchableOpacity key={'available-task-slot-'+i} style={Styles.user_outter_container} onPress={_onSetTask(isAvailable, u, d, taskToBeMoved)}>
                                                <View style={Styles.user_task_container}>
                                                    <Text>Avble</Text>
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    }
                                    
                                    return (
                                        <View key={'nothing-'+i} style={Styles.user_outter_container}>
                                            <View style={Styles.user_task_container}></View>
                                        </View>
                                    )
                                })
                            }
                            </View>
                        </View>
                    )
                })
            }


        </View>
    )
}

export default UserTaskAssignment;