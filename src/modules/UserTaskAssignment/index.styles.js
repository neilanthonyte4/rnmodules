import {StyleSheet, Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

const useStyles = () => {
    return StyleSheet.create({
        container: {
            flex: 1,
            alignItems: 'center'
        },
        days_container: {
            width,
            height: 50,
            flexDirection: 'row',
            borderBottomWidth: 1,
        },
        add_task_btn_container: {
            width,
            height: 50,
            flexDirection: 'row',
            borderBottomWidth: 1,
        },
        task_indicator_container: {
            width,
            flexDirection: 'row',
            borderBottomWidth: 1,
        },
        day_container: {
            width: width / 8,
            height: 50,
            justifyContent: 'center',
            alignItems: 'center',
            borderRightWidth: 1
        },
        outter_outter_continer: {
            borderWidth: 1,
            width: width / 8,
        },
        task_outter_container: (task) => {
            return {
                borderRightWidth: 1,
                width: (width / 8) * 0.98,
                height: task ? 100 : 0,
                justifyContent: 'center',
                alignItems: 'center'
            }
        },
        user_outter_container: {
            borderRightWidth: 1,
            width: width / 8,
            height: 100,
            justifyContent: 'center',
            alignItems: 'center'
        },
        task_container: (task, color = 'orange') => {
            const backgroundColor = task ? color : '#fff';

            return {
                width: (width / 8) * 0.98,
                height: task ? 100 : 0,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor,
            }
        },
        user_list_item: {
            width,
            height: 100,
            borderBottomWidth: 1
        },
        user_task_container: {
            width: (width / 8) * 0.98,
            height: 100,
            borderBottomWidth: 1,
            justifyContent: 'center',
            alignItems: 'center',
        }
    });
}

export default useStyles;