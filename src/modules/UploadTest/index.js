import React from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Dimensions,
  Text,
  Platform,
} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';

const {width} = Dimensions.get('window');

const TestUpload = () => {
  const [token, setToken] = React.useState(
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZmMxZjFlYzMzMWE3NTIyOWZhNjJkYTQ4ZmRjNDIyNWI0ZTA5MzVlZDU2ODA0ZWJlOTI4Yzg1NjE1ZTkzYWM2NGMyOTgyNGFlMzk2NjIzNjkiLCJpYXQiOjE2OTE0NzAzOTQuNjYzNzAzLCJuYmYiOjE2OTE0NzAzOTQuNjYzNzEsImV4cCI6MTcyMzA5Mjc5NC42NDg1MjgsInN1YiI6IjExIiwic2NvcGVzIjpbXX0.YhhFmNg7o4XrAM59gSDyepIPGexUU0nLzRRRbDvOuAdsKlJx27L5twQpTnzVxgq34VEMT5VRlPBsRV7HVz0v-vPK2YgQJgbln1qsRFxOBee7tUkSDpzgf6yEquu23vFZa2ShtGOUv6BmRIC5z2EgZNzaaD_w9nk4Ebz1B9S3Rp1kMM8Ahehhgw6Iwx2Z_Z1A5Nf-toV_qnc_yiE2d8RZyCNNdfTkmLhVVwah17cVgleF1GGwAGYF4KqOhJVD4VtutSnUNyjXOR3NMS2FMbYm6BaWyxUZwzXnHykEwI2yTs-5ZccmlPB2Uw_Gxl9t3foHu5n7lf0HtTZMKOBtiP9ehBAPozkJ7ara9bCj4xA8dSer4EklOuLVbxb2plKmwT68k-MqrlMCokUx_7phStUc8Vw_tzS7gszTDtypbdrAbTSrcSzpyeR_jFp88t0HffUoEmNVufWsTNF8pciScinJ59FsCZjbXyqH-0Rx1AX-vj4-9_sM6jFYHfIebp0DJy-GJ9ejYLZkMKRyQPYqqV-DEXXPqsermsdYC_FdgMRy8ZexZOiwDmU9wc67oDJceZGaSXX1wMvqKMHYAje7npQoWhQ42uQe1JHzjCSxagL3fsLG-_FffHckYiT44HN1dG6F5njzHkc8CbM__Ra_2OZ3si3D8ruHFZSxhbfn5uqeSrs',
  );
  const [endpointURL, setEndpointURL] = React.useState(
    'https://api-dev.mycrowdconnect.com/api/clients/7/files/upload-music',
  );
  const [file, setSelectedFile] = React.useState(null);

  const _onSendRequest = async () => {
    try {
      const realPath =
        Platform.OS === 'ios'
          ? decodeURIComponent(file.uri.replace('file://', ''))
          : file.uri;

      const multipartParams = [
        {
          name: 'file',
          filename: file.name,
          data: RNFetchBlob.wrap(realPath),
        },
        {
          name: 'event_id',
          data: 14,
        },
        {
          name: 'video_id',
          data: 14,
        },
      ];

      RNFetchBlob.fetch(
        'POST',
        endpointURL,
        {
          Authorization: `Bearer ${token}`,
          Accept: 'application/json, text/plain, *.*',
          'Content-Type': 'octet-stream',
        },
        multipartParams,
      )
        .uploadProgress((written, total) => {
          console.log('progress', written / total);
        })
        .then(resp => {
          if (resp.data) {
            console.log('resp', JSON.parse(resp.data));
            const {success} = JSON.parse(resp.data);
            if (success === true) {
              console.log('success');
            }
          }
        })
        .catch(err => {
          console.log('error', err);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const _onSelectFile = async () => {
    try {
      const res = await DocumentPicker.pickSingle({});
      setSelectedFile(res);
    } catch (error) {
      console.log(error);
    }
  };

  const _fetchUpload = async () => {
    try {
      const formData = new FormData();

      const imageFile = {
        uri: file.uri,
        type: file.type,
        name: file.fileName,
        size: file.fileSize,
      };

      formData.append('file', imageFile);
      formData.append('event_id', 14);
      formData.append('video_id', 14);

      const uploadRes = await fetch(endpointURL, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      });

      console.log('RES', await uploadRes.json());
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Token</Text>
      <TextInput
        value={token}
        onChangeText={val => setToken(val)}
        style={styles.input}
      />
      <Text style={styles.label}>Endpoint URL</Text>
      <TextInput
        value={endpointURL}
        onChangeText={val => setEndpointURL(val)}
        style={styles.input}
      />

      {file && file.name ? <Text>{file.name}</Text> : null}
      <TouchableOpacity style={styles.submitBtn} onPress={_onSelectFile}>
        <Text style={styles.btnLabel}>Select File</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.submitBtn} onPress={_onSendRequest}>
        <Text style={styles.btnLabel}>Submit Request</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  input: {width, height: 70, backgroundColor: '#fff', marginBottom: 20},
  label: {
    width,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  submitBtn: {
    width,
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    backgroundColor: 'green',
    marginTop: 20,
  },
  btnLabel: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default TestUpload;
