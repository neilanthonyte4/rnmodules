import {StyleSheet} from 'react-native';
import { utils } from '@hooks';

const useStyles = () => {
    const theme = utils.useTheme();

    return StyleSheet.create({
        container: {
            flex: 1
        },
        time_container: {
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1
        },
        time_val_container: {
            width: 50,
            margin: 5,
            justifyContent: 'center',
            alignItems: 'center',
        },
        time_wrapper: {
            height: 85,
            width: 50,
            backgroundColor: theme.colors.palettes.gray1,
            margin: 5,
            justifyContent: 'center',
            alignItems: 'center',
        },
        time_val_container_sep: {
            height: 85,
            width: 50,
            margin: 5,
            justifyContent: 'center',
            alignItems: 'center',
        },
        time_val: {
            fontSize: 60,
            fontWeight: 'bold',
        },
        increase_btn: {
            backgroundColor: 'red',
            justifyContent: 'center',
            alignItems: 'center',
            width: 40,
            height: 40,
        }
    });
}

export default useStyles;