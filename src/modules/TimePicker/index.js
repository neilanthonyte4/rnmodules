import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import useStyles from './index.styles';
import {IOSStyleModal} from '@components';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { utils } from '@hooks';

export const TimerPicker = () => {
    const theme = utils.useTheme();
    const Styles = useStyles();

    return (
        <View style={Styles.container}>
            <IOSStyleModal {...{
                show: true, 
                contentHeight: 0.3,
                onClose: () => {
                    
                },
                title: 'Select a time',
                rightHeader: 'Save',
                onPressRightHeader: () => {
                     alert('save')
                }
            }}>
                <View style={Styles.time_container}>

                    <View style={Styles.time_val_container}>
                        <TouchableOpacity style={Styles.increase_btn}>
                            <MaterialIcon name='done' size={40} style={{position: 'absolute', padding: 0}} color={theme.colors.palettes.blue3} />
                        </TouchableOpacity>
                        <View style={Styles.time_wrapper}>
                            <Text style={Styles.time_val}>0</Text>
                        </View>
                        <View></View>
                    </View>

                    <View style={Styles.time_val_container}>
                        <Text style={Styles.time_val}>0</Text>
                    </View>
                    <View style={Styles.time_val_container_sep}>
                        <Text style={Styles.time_val}>:</Text>
                    </View>
                    <View style={Styles.time_val_container}>
                        <Text style={Styles.time_val}>3</Text>
                    </View>
                    <View style={Styles.time_val_container}>
                        <Text style={Styles.time_val}>0</Text>
                    </View>
                </View>
            </IOSStyleModal>
        </View>
    )
}