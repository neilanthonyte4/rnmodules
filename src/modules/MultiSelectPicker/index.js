import React from 'react';
import {View, Text, TouchableOpacity, FlatList} from 'react-native';
import useStyle from './index.styles';
import _ from 'lodash';

const MultiSelectPicker = ({options = [
    {label: 'Option 1', value: 'option1'},
    {label: 'Option 2', value: 'option2'},
    {label: 'Option 3', value: 'option3'},
    {label: 'Option 4', value: 'option4'},
    {label: 'Option 5', value: 'option5'}
]}) => {
    const configs = {
        placeholder: 'Select something',
        placeholderColor: '#000',
        dropDownBackgroundColor: 'rgba(0,0,0,0.5)',
        borderRadius: 8,
        listTextColor: '#fff'
    };
    const Styles = useStyle(configs);
    const [selectedOptions, setSelectedOptions] = React.useState([]);
    const [showList, setShowList] = React.useState(false);
    const hasItem = selectedOptions.length > 0 ? true : false;
    
    const _showList = () => {
        setShowList(!showList);
    }
   
    const _onSelectItem = (value) => {
        return () => {
            const newSelectedOptions = [...selectedOptions, value];
            const itemIndex = selectedOptions.indexOf(value);

            if(itemIndex < 0) {
                setSelectedOptions(newSelectedOptions);
            } else {
                const diff = _.difference(newSelectedOptions, [...[], value]);
                setSelectedOptions(diff);
            }
        }
    }

    const renderItem = ({item}) => {
        const isSelected = selectedOptions.indexOf(item.value) > -1 ? true : false;

        return (
            <TouchableOpacity style={Styles.list_item} activeOpacity={0.75} onPress={_onSelectItem(item.value)}>
                <Text style={Styles.item_list_label}>{isSelected ? '✓' : ''} {item.label}</Text>
            </TouchableOpacity>
        )
    }

    return (
        <View style={Styles.container}>
            <TouchableOpacity 
                style={Styles.picker_container_btn} 
                activeOpacity={0.65} 
                onPress={_showList}>
                <View style={Styles.selected_opt_container}>
                    {
                        selectedOptions.length < 1 ? (
                            <Text style={Styles.placeholder}>{configs.placeholder}</Text>
                        ) : selectedOptions.map((opt, index) => (
                            <View key={'opt'+index} style={Styles.opt_label_container}>
                                <Text>{opt}</Text>
                            </View>
                        ))
                    }
                </View>

                <View>
                {
                    showList ? (
                        <View style={Styles.drop_list_container(hasItem)}>
                            <FlatList
                                data={options}
                                renderItem={renderItem}
                                keyExtractor={item => item.value}
                            />
                        </View>
                    ) : null
                }
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default MultiSelectPicker;