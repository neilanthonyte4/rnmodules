import {StyleSheet, Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

const useStyle = (configs) => {
    const DEFAULT_PICKER_CONTAINER_BTN_WIDTH = width * 0.85;
    const DEFAULT_PICKER_CONTAINER_BTN_HEIGHT = 60;

    return StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },
        placeholder: {
            color: configs.placeholderColor
        },
        picker_container_btn: {
            borderWidth: 1,
            width: configs.width ? configs.width : DEFAULT_PICKER_CONTAINER_BTN_WIDTH,
            minHeight: configs.height ? configs.height : DEFAULT_PICKER_CONTAINER_BTN_HEIGHT,
            borderRadius: configs.borderRadius ? configs.borderRadius : 0,
            justifyContent: 'center',
            padding: 10
        },
        drop_list_container: (hasItem) => {
            return {
                width: configs.width ? configs.width : DEFAULT_PICKER_CONTAINER_BTN_WIDTH,
                backgroundColor: configs.dropDownBackgroundColor ? configs.dropDownBackgroundColor : 'rgba(0,0,0,0.5)',
                position: 'absolute',
                zIndex: 5,
                top: hasItem ? 10 : 20,
                left: -10
            }
        },
        list_item: {
            paddingLeft: 10,
            paddingRight: 10,
            paddingBottom: 10,
            paddingTop: 10,
            justifyContent: 'center',
        },
        item_list_label: {
            color: configs.listTextColor ? configs.listTextColor : '#fff'
        },
        selected_opt_container: {
            flexDirection: 'row',
            overflow: 'hidden',
            flexWrap: 'wrap',
        },
        opt_label_container: {
            backgroundColor: 'lightgray',
            margin: 5,
            padding: 5,
            borderRadius: 8
        }
    });
}

export default useStyle;