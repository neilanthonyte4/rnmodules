import React, {useRef, useState, useEffect} from 'react';
import {
  View,
  Dimensions,
  StyleSheet,
  Text,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  Animated,
  PanResponder,
} from 'react-native';
import Video from 'react-native-video';
import SampleVide from './sample_vid.mp4';
import {createThumbnail} from 'react-native-create-thumbnail';
import PlayIcon from './play-icon.png';
import PauseIcon from './pause-icon.png';

const {width} = Dimensions.get('window');
const VID_URL =
  'https://sample-videos.com/video123/mp4/480/big_buck_bunny_480p_10mb.mp4';

const createThumbs = async (
  numerOfThumbs,
  videoEndTime,
  setvideoThumbnails,
) => {
  try {
    const newThumbArr = [];

    if (videoEndTime < 1) return newThumbArr;

    const getTimeInterval = videoEndTime / numerOfThumbs;

    let timeStampPerInterval = getTimeInterval;

    for (let t = 0; t < numerOfThumbs; t += 1) {
      const thumb = await createThumbnail({
        url: VID_URL,
        timeStamp: t === 0 ? 1000 : timeStampPerInterval * 1000,
        format: 'jpeg',
      });

      newThumbArr.push({
        id: `thumb${t}`,
        timeStamp: timeStampPerInterval,
        thumbnail: thumb,
      });
      timeStampPerInterval = timeStampPerInterval += getTimeInterval;
    }

    setvideoThumbnails(newThumbArr);
  } catch (error) {
    throw error;
  }
};

const App = () => {
  const NUMBER_OF_THUMBS = 5;

  const videoRef = useRef();
  const doneLoading = useRef(false);
  const animatedProgress = useRef(new Animated.Value(0)).current;

  const [currentVideoTime, setCurrentVideoTime] = useState(0);
  const [videoEndTime, setVideoEndTime] = useState(0);
  const [videoThumbnails, setvideoThumbnails] = useState([
    {id: 'thumb1', timeStamp: 0, thumbnail: null},
    {id: 'thumb2', timeStamp: 0, thumbnail: null},
    {id: 'thumb3', timeStamp: 0, thumbnail: null},
    {id: 'thumb4', timeStamp: 0, thumbnail: null},
    {id: 'thumb5', timeStamp: 0, thumbnail: null},
  ]);
  const [mute, setMute] = useState(true);
  const [videoEnded, setVideoEnd] = useState(true);
  const [videoPaused, setVideoPaused] = useState(false);

  const pBarDrag = useRef(
    PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderGrant: (e, gestureState) => {},
      onPanResponderMove: (e, gestureState) => {},
      onPanResponderRelease: (e, gestureState) => {},
    }),
  );

  const pBarStyle = styles.videoProgressBar(
    currentVideoTime,
    videoEndTime,
    animatedProgress,
    NUMBER_OF_THUMBS,
  );
  const thumbContainer = styles.thumbContainer(NUMBER_OF_THUMBS);
  const thumbStyle = styles.thumbStyle(NUMBER_OF_THUMBS);
  const pbarIndicator = styles.pbarIndicator(NUMBER_OF_THUMBS);
  const videoProgressBarContainer =
    styles.videoProgressBarContainer(NUMBER_OF_THUMBS);

  useEffect(() => {
    createThumbs(NUMBER_OF_THUMBS, videoEndTime, setvideoThumbnails);
  }, [videoEndTime, setvideoThumbnails]);

  useEffect(() => {
    if (videoEndTime > 0) {
      pBarDrag.current = PanResponder.create({
        onMoveShouldSetResponderCapture: () => true,
        onMoveShouldSetPanResponderCapture: () => true,
        onPanResponderGrant: (e, gestureState) => {
          // console.log('on grant', gestureState);
        },
        onPanResponderMove: (e, gestureState) => {
          const {moveX} = gestureState;
          const getWidthPercent = moveX / width;
          const getSeekTime = videoEndTime * getWidthPercent;

          if (videoRef.current) videoRef.current.seek(getSeekTime);
        },
        onPanResponderRelease: (e, gestureState) => {
          // console.log('on release', gestureState);
        },
      });
    }
  }, [videoEndTime]);

  const _onPlayAgain = () => {
    setVideoEnd(false);
    setVideoPaused(false);
    setTimeout(() => {
      if (videoRef.current) videoRef.current.seek(0);
    }, 200);
  };

  const _onPauseVideo = () => {
    setVideoPaused(!videoPaused);
  };

  if (VID_URL === '' || typeof VID_URL === 'undefined' || VID_URL === null) {
    return (
      <View style={styles.videoContainer}>
        <View style={styles.videoContainer}>
          <View style={styles.videoLoader}>
            <ActivityIndicator color="#fff" size="large" />
            <Text style={styles.noVidLabel}>No video found</Text>
          </View>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.videoContainer}>
      <View style={styles.videoContainer}>
        {!videoThumbnails[0].thumbnail ? (
          <View style={styles.videoLoader}>
            <ActivityIndicator color="#fff" size="large" />
          </View>
        ) : null}
        {videoThumbnails[0].thumbnail && videoEnded ? (
          <TouchableOpacity
            style={styles.videoPlayOverlay}
            onPress={_onPlayAgain}>
            <Image source={PlayIcon} style={styles.playIcon} />
          </TouchableOpacity>
        ) : null}
        {videoEnded ? null : (
          <TouchableOpacity
            style={
              videoPaused
                ? styles.videoPauseOverlayActive
                : styles.videoPauseOverlay
            }
            onPress={_onPauseVideo}>
            {videoPaused ? (
              <Image source={PauseIcon} style={styles.playIcon} />
            ) : null}
          </TouchableOpacity>
        )}
        <Video
          repeat={false}
          source={{uri: VID_URL}} // Can be a URL or a local file.
          ref={videoRef} // Store reference
          onBuffer={() => {}} // Callback when remote video is buffering
          onError={() => {}} // Callback when video cannot be loaded
          style={styles.videoContainer}
          muted={mute}
          paused={videoPaused}
          onLoad={payload => {
            setVideoEndTime(payload.duration);
          }}
          onEnd={() => {
            setVideoEnd(true);
          }}
          onProgress={currentProgress => {
            const {currentTime, seekableDuration} = currentProgress;

            if (currentTime > 0 && videoEnded === true) {
              setVideoEnd(false);
            }

            if (
              videoThumbnails[0].thumbnail &&
              currentTime > 0 &&
              doneLoading.current === false
            ) {
              doneLoading.current = true;
              videoRef.current.seek(0);
              setMute(false);
              return;
            }

            if (videoThumbnails[0].thumbnail && doneLoading.current === true) {
              // setCurrentVideoTime(currentTime);

              // FOR ANIMATED PBAR
              const pBarPercent = currentTime / seekableDuration;
              const pBarWidth = width * pBarPercent;

              Animated.timing(animatedProgress, {
                toValue: pBarWidth,
                duration: 0,
                delay: 0,
                useNativeDriver: false,
              }).start();
            }
          }}
          disableFocus={true}
        />
      </View>
      <View style={videoProgressBarContainer}>
        {/* <View style={pBarStyle}></View> */}
        <Animated.View style={pBarStyle} {...pBarDrag.current.panHandlers}>
          <View style={pbarIndicator}>
            <View style={styles.pbarIndicatorLeft} />
            <View style={styles.pbarIndicatorRight} />
          </View>
        </Animated.View>
        {videoThumbnails.map(t => {
          return (
            <View key={t.id} style={thumbContainer}>
              {t.thumbnail ? (
                <Image
                  source={{uri: t.thumbnail.path}}
                  resizeMode="stretch"
                  style={thumbStyle}
                />
              ) : (
                <ActivityIndicator size="small" color="#000" />
              )}
            </View>
          );
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  videoProgressBarContainer: numOfThumb => {
    return {
      width,
      height: width / numOfThumb,
      backgroundColor: 'lightgray',
      flexDirection: 'row',
    };
  },
  videoContainer: {
    width,
    height: width / 2,
    backgroundColor: '#000',
  },
  videoLoader: {
    width,
    height: width / 2,
    backgroundColor: '#000',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  videoPlayOverlay: {
    width,
    height: width / 2,
    backgroundColor: 'rgba(0,0,0,0.5)',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  videoPauseOverlay: {
    width,
    height: width / 2,
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  videoPauseOverlayActive: {
    width,
    height: width / 2,
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 9,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  videoProgressBar: (
    currentTime,
    videoEndTime,
    animatedProgress,
    numOfThumb,
  ) => {
    if (videoEndTime > 0) {
      const pBarPercent = currentTime / videoEndTime;
      const pBarWidth = width * pBarPercent;

      return {
        // width: pBarWidth,
        width: animatedProgress,
        height: width / numOfThumb,
        // backgroundColor: 'rgba(37, 150, 190,0.5)',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 9,
        justifyContent: 'center',
        alignItems: 'flex-end',
        // overflow: 'hidden',
        borderRightWidth: (width / numOfThumb) * 0.35,
        borderColor: 'rgba(255,255,255,0)',
      };
    }

    return {
      width: 0,
      height: width / numOfThumb,
      // backgroundColor: 'rgba(37, 150, 190,0.5)',
      position: 'absolute',
      top: 0,
      left: 0,
      zIndex: 9,
      justifyContent: 'center',
      alignItems: 'flex-end',
      // overflow: 'hidden',
    };
  },
  thumbContainer: numOfThumb => {
    return {
      height: width / numOfThumb,
      width: width / numOfThumb,
      borderWidth: 1,
      justifyContent: 'center',
      alignItems: 'center',
      overflow: 'hidden',
    };
  },
  thumbStyle: numOfThumb => {
    return {
      height: 100,
      width: width / numOfThumb,
    };
  },
  playIcon: {
    width: (width / 2) * 0.65,
    height: (width / 2) * 0.65,
  },
  pbarIndicator: numOfThumb => {
    return {
      height: width / numOfThumb,
      width: (width / numOfThumb) * 0.35,
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      flexDirection: 'row',
      position: 'absolute',
      zIndex: 99,
      right: 0 - (width / numOfThumb) * 0.35,
    };
  },
  pbarIndicatorLeft: {
    width: '50%',
    height: '100%',
    borderRightWidth: 1,
    borderColor: 'rgba(255,255,255,0.5)',
  },
  pbarIndicatorRight: {
    width: '50%',
    height: '100%',
    borderLeftWidth: 1,
    borderColor: 'rgba(255,255,255,0.5)',
  },
  noVidLabel: {
    color: '#fff',
  },
});

export default App;
