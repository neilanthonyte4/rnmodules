import React from "react";
import {View, NativeModules, TouchableOpacity, Text, Platform} from 'react-native';
import useStyle from "./index.styles";

const WifiScan = () => {
    const Styles = useStyle();
    const {WifiModule: {scanWifi}} = NativeModules;
    const [wifiCons, setWifiCons] = React.useState([]);

    const _onScanWifi = () => {
        const onSuccess = (res) => {
            if(res.length < 1) {
                alert(
                    'No Wifi Available. Make sure to turn on your location and enable the permission.'
                )
            } else {
                setWifiCons(res);
            }
        }
        const onError = (err) => {
            alert(`error ${err}`)
        }


        if(Platform.OS === 'android') scanWifi(onSuccess, onError);
        if(Platform.OS === 'ios') scanWifi((val) => console.log('hi', val));
    }

    return (
        <View style={Styles.container}>
            <TouchableOpacity onPress={_onScanWifi} style={Styles.scan_btn}>
                <Text style={Styles.btn_label}>Scan Wifi</Text>
            </TouchableOpacity>

            <Text>Wifi Connections:</Text>
            {
                wifiCons.length < 1 ? <Text>No Connection Available</Text> : wifiCons.map((item, index) => {
                    return (
                        <Text key={index}>{item}</Text>
                    )
                })
            }
        </View>
    )
}

export default WifiScan;