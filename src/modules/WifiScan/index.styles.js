import {StyleSheet, Dimensions} from 'react-native';

const useStyle = () => {
    const {width} = Dimensions.get('window');
    const PICKER_WIDTH = width * 0.9;

    return StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },
        scan_btn: {
            backgroundColor: '#1670f7',
            width: PICKER_WIDTH * 0.9,
            height: 50,
            marginBottom: 15,
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
        },
        btn_label: {
            fontWeight: 'bold',
            color: '#fff'
        },
    })
}

export default useStyle;