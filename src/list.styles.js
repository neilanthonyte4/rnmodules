import {StyleSheet, Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

const useStyle = () => {
    return StyleSheet.create({
        container: {
            flex: 1
        },
        item_module: {
            width,
            height: 50,
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 10,
            borderBottomWidth: 1,
            borderBottomColor: '#d9d9d9'
        }
    });
}

export default useStyle;