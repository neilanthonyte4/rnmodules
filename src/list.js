import React from 'react';
import {View, FlatList, Text, TouchableOpacity} from 'react-native';
import Modules from './modules';
import useStyle from './list.styles';

const list = Object.entries(Modules);

const ModuleList = (props) => {
    const Styles = useStyle();
    const {navigation} = props;

    const _onSelectModule = (routeName) => {
        navigation.navigate(routeName);
    }

    return (
        <View style={Styles.container}>
            <FlatList
                data={list}
                keyExtractor={key => key[0]}
                renderItem={({item}) => {
                    const MODULE_NAME = item[0];

                    return (
                        <TouchableOpacity style={Styles.item_module} onPress={() => _onSelectModule(MODULE_NAME)}>
                            <Text>{MODULE_NAME}</Text>
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}

export default ModuleList;