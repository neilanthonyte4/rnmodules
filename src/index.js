import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import List from './list';
import Modules from './modules';

const Stack = createNativeStackNavigator();

const RNModules = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Module List" component={List} />
        <Stack.Screen
          name="RickDatePicker"
          component={Modules.RickDatePicker}
        />
        <Stack.Screen name="WifiScan" component={Modules.WifiScan} />
        <Stack.Screen
          name="MultiSelectPicker"
          component={Modules.MultiSelectPicker}
        />
        <Stack.Screen
          name="UserTaskAssignment"
          component={Modules.UserTaskAssignment}
        />
        <Stack.Screen
          name="RadialGradient"
          component={Modules.RadialGradient}
        />
        <Stack.Screen
          name="CustomVideoPlayer"
          component={Modules.CustomVideoPlayer}
        />
        <Stack.Screen name="CameraOverlay" component={Modules.CameraOverlay} />
        <Stack.Screen name="TestUpload" component={Modules.TestUpload} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RNModules;
